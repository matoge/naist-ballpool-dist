/*
Class ragdool

"ragdoll_bodies" is the list of pointers of all bodies.
All items are added by svgbox script.
*/

#pragma once

#include <Box2D/Box2D.h>
#include <XnCppWrapper.h>

#include <vector>
#include <math.h>

#include "uData.h"

using namespace std;


//#define DEBUG
#define WW 15.0f
#define WH 12.5f
#define WSCALE 2.5f // Controls X range of robots
#define YOFFSET -8.0f // Position offsets of robots
	
#define MAX_USERS 32

#define	RAND_LIMIT	32767

#define RAGDOLL_ROBOT 0
#define RAGDOLL_HUMAN 1

// Dolls
#define MAX_DOOLS 2

//openNI
#define NI_W 640.0f
#define NI_H 480.0f

// Games

#define INITIAL_SCORE 99
#define SCORE_OUT_OF_GAME -1
#define CLEAR_NFRAMES 777
#define ETIME 7777

#define XN_CALIBRATION_FILE_NAME "UserCalibration.bin"



struct Polar{
	Polar(){
		angle = 0.0f;
		radius = 0.0f; 
	}
	Polar(float32 angle, float32 radius) : angle(angle), radius(radius) {}
	float angle;
	float radius;
	b2Vec2 getReal(){
		return b2Vec2( radius* cos(angle), radius * sin(angle) );
	}
};

class ragdoll
{
public:
	ragdoll(b2World* m_world, b2Body* gB, int dolltype, int dollid);

	void update(xn::UserGenerator* pUserGenerator,xn::DepthGenerator* pDepthGenerator);
	void MoveLimb(XnUserID player, XnSkeletonJoint eJoint1, XnSkeletonJoint eJoint2);

	vector<b2Body*> ragdoll_bodylist;
	bool isfree();
	// pointers to all bodies
	//vector<b2Body*> ragdoll_bodies; 

	// robot


	void mj_looseall();
	XnUserID uid; // Corresponding User ID
	int doolid;
private:
	int dooltype;

	b2Body* groundBody;
	b2MouseJoint* mj_head;
	b2MouseJoint* mj_neck;

	b2MouseJoint* mj_torso;

	b2MouseJoint* mj_rightshoulder;
	b2MouseJoint* mj_leftshoulder;

	b2MouseJoint* mj_righthand;
	b2MouseJoint* mj_rightelbow;
	b2MouseJoint* mj_lefthand;
	b2MouseJoint* mj_leftelbow;

	b2MouseJoint* mj_leftweist;
	b2MouseJoint* mj_rightweist;

	b2MouseJoint* mj_rightknee;
	b2MouseJoint* mj_rightfoot;
	b2MouseJoint* mj_leftknee;
	b2MouseJoint* mj_leftfoot;

	float torso_width;
	float torso_height;
	b2Vec2 torso_center;
	b2Vec2 torso_init_extent;
	
	// stores relative polar points of all the parts of the ragdoll
	vector<Polar> m_pPoints;

	// ground offset. determined when first caliburated
	float goffset;
	bool isCaliburated;

	// default position
	b2Vec2 defPos[2];

	b2Vec2 kinect_to_box2d(XnPoint3D input);

	void mj_intialize();
	void mj_destroy();
};
