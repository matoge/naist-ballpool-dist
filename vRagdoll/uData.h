#pragma once

// user Data structure of bodies.
struct uData{
	uData(){
		name="ddd";
		fill= b2Color(1.0f,1.0f,1.0f);
		stroke= b2Color(1.0f,1.0f,1.0f);
		strokewidth = 10.0f;
	}
	std::string name;
	b2Color fill;
	b2Color stroke;
	float strokewidth;
};