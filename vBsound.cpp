#include "vBsound.h"

using namespace std;
//Sound
vBsound::vBsound(void)
{
	sndnum = 0;
	/*SE*/
	mciSendString("open sounds\\ef_caliburation.mp3 alias ef_cal",NULL,0,NULL);
	mciSendString("open sounds\\ef_jingle.mp3 alias ef_congrat",NULL,0,NULL);
	mciSendString("open sounds\\ef_cdown.mp3 alias ef_cdown",NULL,0,NULL);
	mciSendString("open sounds\\fireworks.mp3 alias ef_fireworks",NULL,0,NULL);
	/*BGM*/
	string tnames[MAXSND]={"freerun.mp3",  "ad.mp3","benny.mp3","ac.mp3", "ab.mp3", "ae.mp3", "aa.mp3", "af.mp3"};
	//for(int i=0; i<MAXSND;i++){
	//	fnames[i] = tnames[i];
	//	string hoge = "open sounds\\" + fnames[i] + " alias " + fnames[i];
	//	cout << hoge << endl;
	//	mciSendString(hoge.c_str(),NULL,0,NULL);
	//}
}

vBsound::~vBsound(void){
	//close all
		/*SE*/
	mciSendString("close sounds\\ef_caliburation.mp3",NULL,0,NULL);
	mciSendString("close sounds\\ef_jingle.mp3",NULL,0,NULL);
	mciSendString("close sounds\\ef_cdown.mp3",NULL,0,NULL);
	mciSendString("close sounds\\ef_fireworks.mp3",NULL,0,NULL);
	/*BGM*/	for(int i=0; i<MAXSND;i++){
		string hoge = "close sounds\\" + fnames[i];
		cout << hoge << endl;
		mciSendString(hoge.c_str(),NULL,0,NULL);
	}
}

void vBsound::playBGM(){
	id = mciGetDeviceID(fnames[0].c_str());
	string cmd = "play "+fnames[sndnum%MAXSND]+" from 0";
	mciSendString(cmd.c_str(),NULL,0,NULL);
}

void vBsound::continueBGM(){
	mciStatus.dwItem = MCI_STATUS_MODE;
	mciSendCommand(id, MCI_STATUS, MCI_STATUS_ITEM, (DWORD)&mciStatus);
	if(mciStatus.dwReturn != MCI_MODE_PLAY )
	{
		string cmd = "close " + fnames[sndnum%MAXSND];
		//mciSendString(cmd.c_str(),NULL,0,NULL);
		sndnum++;
		cmd = "play "+fnames[sndnum%MAXSND]+" from 0";
		mciSendString(cmd.c_str(),NULL,0,NULL);
		id = mciGetDeviceID(fnames[sndnum%MAXSND].c_str());
	}

}

void vBsound::playSE(string alias){
	id_se=mciGetDeviceID(alias.c_str());

	mciStatus.dwItem = MCI_STATUS_MODE;
	mciSendCommand(id_se, MCI_STATUS, MCI_STATUS_ITEM, (DWORD)&mciStatus);
	if(mciStatus.dwReturn != MCI_MODE_PLAY )
	{
		string cmd = "play " + alias + " from 0";
		mciSendString(cmd.c_str(),NULL,0,NULL);
	}
}