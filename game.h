#pragma once

#include <Box2D/Box2D.h>
#include "Render.h"
#include <vector>
#include <iostream>
#include "uData.h"
#include "constants.h"

using namespace std;

class vGame
{
public:
	vGame(int nProblem);
	~vGame(void);
	b2World* m_world;
	DebugDraw* m_debugDraw;
	b2Body* groundBody;
	b2Body* wallBody;
	void DestroyProblem();
	void DrawColor();
	void DrawShape2(b2Fixture* fixture, const b2Transform& xf, const b2Color& color, const b2Color& color2, const float linewidth);
	bool isMatched(vector<b2Body*> prb_list);
	int max_score;
	int eTime;

	vector<b2Body*> ans_list;
	
};

