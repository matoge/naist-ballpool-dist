#include <iostream>
#include <stdlib.h> // Necessary for "exit" function

#include <Box2D/Box2D.h>
#include "vBallpool.h"
#include <freeglut/GL/glut.h>

#include "ni.h"
#include "constants.h"
#include "game.h"
#include "vBsound.h"

#ifdef USE_FTGL
#include "ftgl.h"
#endif

#define INTERVAL 0

/* Global variables:

	OpenNI uses old-fashioned callback function defined in global scope.
	Some global variable should be hided into the "game" class
*/

vBallpool* vB;
vBsound* vS;
vGame* pGame;

int nframes=0;
int gScore=INITIAL_SCORE;
int nClearFrame = -1;
int nProblem = -1;

int iMW;
int iSubwindow, iSubWindow2;
bool bShowKinect=true;
bool bShowAnswer=false;
bool bFullScreen=true;
float ZoomRate=0.0f;
void startGame();

double speed[MAX_USERS];
double prevPos[MAX_USERS];
int userframes[MAX_USERS];

void CleanupExit()
{
	g_Context.Release();
	g_DepthGenerator.Release();
	g_UserGenerator.Release();
	vS->~vBsound();
	exit (1);
}

//Called when a key is pressed
void handleKeypress(unsigned char key, //The key that was pressed
					int x, int y)//The current mouse coordinates
{
	switch (key) {
		// see http://www.alt-codes.net/ for key codes
		case 27: //Escape key
			CleanupExit();
			break;
		case 'd': //d
			if(bShowKinect==true){
				glutSetWindow(iSubwindow);
				glutHideWindow();
				bShowKinect = !bShowKinect;
				glutSetWindow(iMW);
			}
			else{
				glutSetWindow(iSubwindow);
				glutShowWindow();
				bShowKinect = !bShowKinect;
				glutSetWindow(iMW);
			}
			break;
		case 's': //s
			vB->dropTitle();
			break;
		case 'q': 
			vB->dropStar();
			break;
		case 97: //a
			vB->dropCircle();
			break;
		case '1': //a
			vB->fireworks();
			break;
		case 99: //c
			startGame();
			break;
		case 122: //z
			vB->dropAll();
			break;
		case 'e':
			if(bShowAnswer==true){
				glutSetWindow(iSubWindow2);
				glutHideWindow();
				glutSetWindow(iMW);
			}
			else{
				glutSetWindow(iSubWindow2);
				glutShowWindow();
				glutSetWindow(iMW);
			}
			bShowAnswer = !bShowAnswer;
			break;
		case 'f': //f
			if(bFullScreen){
				glutReshapeWindow(800, 600);
				glutPositionWindow(0,0);
				glutReshapeWindow(800, 600);
				bFullScreen=false;
			}
			else{
				glutFullScreen();
				bFullScreen=true;
			}
			break;
		case 105:
			vB->changeGravity(0.0f,0.1f);
			break;
		case 109:
			vB->changeGravity(0.0f,-0.1f);
			break;
		case 106:
			vB->changeGravity(0.1f,0.0f);
			break;
		case 107:
			vB->changeGravity(-0.1f,0.1f);
			break;
		case 114:
			vB->resetGravity();
			break;
		case 'S':
			LoadCalibration();
			break;


	} 
}

//Initializes 3D rendering
void initRendering() {
	//Makes 3D drawing work when something is in front of something else
	glDisable(GL_DEPTH_TEST);
}

void glUpdate(int value) {
	//Tell GLUT to call update again in 25 milliseconds
	glutTimerFunc(INTERVAL, glUpdate, 0);
	//glutPostRedisplay(); //Tell GLUT that the scene has changed
}
//Called when the window is resized
void handleResize(int w, int h) {
	//Tell OpenGL how to convert from coordinates to pixel values
	//Tell OpenGL how to convert from coordinates to pixel values
	glViewport(0, 0, w, h);

	glMatrixMode(GL_MODELVIEW); //Switch to setting the camera perspective

	//Set the camera perspective
	glLoadIdentity(); //Reset the camera

	float rate = float(h)/float(w);

	b2Vec2 center(0.0f, rate*WW);
	b2Vec2 extents(WW, rate*WW);
	b2Vec2 lower = center - extents;
	b2Vec2 upper = center + extents;

	ZoomRate = float(w)/float(WW);
	//cout << ZoomRate << endl;
	glLineWidth(0.05f*float(w)/float(WW));
	gluOrtho2D(lower.x, upper.x, lower.y, upper.y);

	// put subwindows to corners
	glutSetWindow(iMW);
	int wCxt[4] = {glutGet(GLUT_WINDOW_X),glutGet(GLUT_WINDOW_Y),glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT)};
	
	glutSetWindow(iSubwindow);
	glutPositionWindow(20, 20);
	glutReshapeWindow( wCxt[2]/6, wCxt[2]*3/24 );

	glutSetWindow(iSubWindow2);
	glutPositionWindow(wCxt[2] - wCxt[2]/4, 16);
	glutReshapeWindow( wCxt[2]/4, wCxt[2]*3/16 );
	glutSetWindow(iMW);

}

void handleResize2(int w, int h) {
	//Tell OpenGL how to convert from coordinates to pixel values
	glViewport(0, 0, w, h);

	glMatrixMode(GL_MODELVIEW); //Switch to setting the camera perspective

	//Set the camera perspective
	glLoadIdentity(); //Reset the camera

	float rate = float(h)/float(w);

	b2Vec2 center(0.0f, rate*WW);
	b2Vec2 extents(WW, rate*WW);
	b2Vec2 lower = center - extents;
	b2Vec2 upper = center + extents;

	ZoomRate = float(w)/float(WW);
	//cout << ZoomRate << endl;
	glLineWidth(0.05f*float(w)/float(WW));
	gluOrtho2D(lower.x, upper.x, lower.y, upper.y);
}


void DrawString3D(GLfloat x, GLfloat y, char *str, float scale)
{
	glPushMatrix();
	glTranslatef(x,y,0.0f);
	glScalef(scale,scale,1.0f);

	while(*str){
		glutStrokeCharacter(GLUT_STROKE_ROMAN, *str);
		++str;
	}
	glPopMatrix();
}

#ifdef USE_FTGL
void DrawStringFTGL(GLfloat x, GLfloat y, char *str, float scale, float colorR, float colorG, float colorB)
{
    int renderMode = FTGL::RENDER_FRONT | FTGL::RENDER_BACK;
	glPushMatrix();
	glColor3f(colorR,colorG,colorB);
	glRasterPos2i(x,y);

	int width = glutGet(GLUT_WINDOW_WIDTH);
	fonts[FTGL_PIXMAP]->FaceSize(int(width/4*scale));
	fonts[FTGL_PIXMAP]->Render(str, -1,FTPoint(), FTPoint(), renderMode);
	
	glPopMatrix();
}
#endif

void glutIdle (void)
{
	if (g_bQuit) {
		CleanupExit();
	}
	// Display the frame
	glutPostRedisplay();
}


void drawScene() {

#if _DEBUG
	system("cls");
#endif

	//Clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor( 250.0f/255, 250.0f/255, 250.0f/255, 1);
	//glClearColor( 0, 0, 0, 1);

	glMatrixMode(GL_PROJECTION); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective

	// Check if position is matched
	if( vB->cake_bodylist.size() != 0 && nClearFrame==-1){
		if(pGame->isMatched(vB->cake_bodylist)){
			gScore -=1;
			vS->playSE("ef_cdown");
		}
		if( bShowAnswer ){
			char c[2];
			char d[10];
			char et[10];
			sprintf(c,"%2d",gScore);
			sprintf(d,"PROBLEM %2d",(nProblem+1));
			sprintf(et,"TIME %4d", pGame->eTime);
#ifdef USE_FTGL
			DrawStringFTGL(-4.0f,8.0f,c,1.0f, 1.0f, 0.2f, 0.5f);
			DrawStringFTGL(-5.0f,14.0f,d,0.3f, 0.0f, 0.7f, 0.5f);
			DrawStringFTGL(-5.5f,5.0f,et,0.3f, 0.1f, 0.2f, 1.0f);
#else
			DrawString3D(-0.1f,0.15f,c,0.05f);
#endif
		}
		pGame->eTime--;
		if(pGame->eTime == 0)
			 startGame(); //Time out
	}

	// FireWorks
	if( 0 < nClearFrame && nClearFrame % 30 == 0){
		vB->fireworks();
	}else if(nClearFrame == CLEAR_NFRAMES-90){
		for(int i=0;i<15;i++){
			vB->dropCircle();
			vB->dropStar();
		}
	}
	if( gScore == 0){
		// Game success !! Draw congratulation scenes
		nClearFrame = CLEAR_NFRAMES;
		gScore = SCORE_OUT_OF_GAME; // out of game
		vB->destroyCake();
		vS->playSE(string("ef_congrat"));

	}
	if( nClearFrame == 0){
			vB->dropAll();
			// Create new problem

			nProblem = (nProblem+1)%MAX_PROBLEM;
			vB->createCake(nProblem);
			pGame->~vGame();
			pGame = new vGame(nProblem);
			gScore = pGame->max_score;

			nClearFrame = -1;
	}else if(nClearFrame>0){
			nClearFrame --; 
	}


	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);
	glViewport(0 , 0 , width , height);

	// Keep max number of balls
	while (vB->title_bodylist.size() > MAX_BALLS){
		vB->destroyOne();
	}
	vector<b2Body*>::iterator it = vB->fireworks_bodylist.begin();
	while (vB->fireworks_bodylist.size() > MAX_FIRES){
		vB->m_world->DestroyBody(*it);
		vB->fireworks_bodylist.erase(it);
	}

	// Prepare for simulation
	vB->Step(TIMESTEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

	// Update skeleton
	if (!g_bPause)
	{
		// Read next available data
		g_Context.WaitAndUpdateAll();
	}
	vB->UpdateSkeletons();

	// not in game mode
	// Drop circles and stars constantly
	if( nClearFrame == -1 && nframes%90 == 0){
			vB->dropCircle();
			vB->dropCircle();
			vB->dropCircle();
			vB->dropStar();
	}

	glutSwapBuffers(); //Send the 3D scene to the screen
	glutPostRedisplay(); //Tell GLUT that the scene has changed
}


// Display function for problem
void glutDisplay2(void){

#if _DEBUG
	system("cls");
#endif

#ifdef USE_FTGL
	//Clear information from last draw
	//glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT );
	glClearColor( 1.0f, 0.9f, 0.9f, 0.5);

	glMatrixMode(GL_PROJECTION); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective

	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);
	glViewport( 0 , 0 , width *1.2 , height *1.2);
	if( nframes/10%2 == 0 ){
		DrawStringFTGL(-10.0f,0.1f,"ANSWER",0.8f, 1.0f, 0.2f, 0.5f);
	}else{
		DrawStringFTGL(-10.0f,0.1f,"ANSWER",0.8f, 0.2f, 0.5f, 1.0f);
	}
#endif

	pGame->DrawColor();

	glutSwapBuffers(); //Send the 3D scene to the screen
	glutPostRedisplay(); //Tell GLUT that the scene has changed
}


// this function is called each frame
void glutDisplay (void)
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Setup the OpenGL viewpoint
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	xn::SceneMetaData sceneMD;
	xn::DepthMetaData depthMD;
	g_DepthGenerator.GetMetaData(depthMD);
	g_UserGenerator.GetUserPixels(0, sceneMD);
	glOrtho(0, depthMD.XRes(), depthMD.YRes(), 0, -1.0, 1.0);
	
	glDisable(GL_TEXTURE_2D);

	DrawDepthMap(depthMD, sceneMD);

	glutSwapBuffers();
    glutPostRedisplay(); //Tell GLUT that the scene has changed

	/* Sound */
	vS->continueBGM(); 

}



void glInit (int * pargc, char ** argv)
{
	glutInit(pargc, argv);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(640, 480); //Set the window size

	//Create the window
	iMW = glutCreateWindow("NAIST Ballpool");

	//Set handler functions for drawing, keypresses, and window resizes
	glutDisplayFunc(drawScene);
	glutKeyboardFunc(handleKeypress);
	glutReshapeFunc(handleResize);


	glutTimerFunc(INTERVAL, glUpdate, 0); //Add a timer
	glutIdleFunc(glutIdle);

	glEnableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	// antialias
	glEnable(GL_LINE_SMOOTH);

	glutFullScreen();

	int wid1 = glutGet(GLUT_WINDOW_WIDTH);
	// SubWindow for kinect
	iSubwindow = glutCreateSubWindow(iMW, 20, 20, wid1/9,wid1/12);
	bShowKinect=true; // for toggle hide/show 
	glutDisplayFunc(glutDisplay);
	glutTimerFunc(INTERVAL, glUpdate, 0); //Add a timer
	glutIdleFunc(glutIdle);
	glEnableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);


	// SubWindow for game
	iSubWindow2 = glutCreateSubWindow(iMW, wid1-wid1/6, 20,wid1/6,wid1/8);
	glutDisplayFunc(glutDisplay2);
	glutTimerFunc(INTERVAL, glUpdate, 0); //Add a timer
	glutReshapeFunc(handleResize2);
	glutIdleFunc(glutIdle);
	glutHideWindow();
}

void startGame(){
	nProblem = (nProblem+1)%MAX_PROBLEM;
	vB->createCake(nProblem);
	pGame->~vGame();
	pGame = new vGame(nProblem);
	gScore = pGame->max_score;
	pGame->eTime = ETIME;

	glutSetWindow(iSubWindow2);
	glutShowWindow();
	glutSetWindow(iMW);
	bShowAnswer=true;
}

//int WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPSTR str,int nWinMode)
int main(int argc, char** argv)
{

	printf("Initializing Kinect...");
	// kinect Initialize
	ni_initialize(argc, argv);
	//sound

	vS = new vBsound();
	vS->playBGM();
	//Initialize GLUT
	glInit(&argc, argv);

	// Construct vBallpool Box2D object
	vB = new vBallpool();

	// init game
	pGame = new vGame(0);

	// init fonts
#ifdef USE_FTGL
    setUpFonts(fontfile);	
#endif
	//startGame();

	for( int i=0; i<MAX_USERS; i++ ) {
		prevPos[i] = 0;
		speed[i] = 0;
		userframes[i] = 0;
	}

	glutMainLoop(); //Start the main loop.  glutMainLoop doesn't return.
	return 0; //This line is never reached


}