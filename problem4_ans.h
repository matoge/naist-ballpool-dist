vector<b2Body*> bodylist;

//
//rect2990
//
b2BodyDef rect2990_bdef;
rect2990_bdef.type = b2_dynamicBody;
rect2990_bdef.bullet = false;
rect2990_bdef.position.Set(-2.79678217416f,10.4033460935f);
rect2990_bdef.angle=-0.0f;
rect2990_bdef.linearDamping = 0.3f;
rect2990_bdef.angularDamping =0.3f;
b2Body* rect2990_body = m_world->CreateBody(&rect2990_bdef);

b2PolygonShape rect2990_ps;
rect2990_ps.SetAsBox(2.11488001521f, 2.11488001521f);

b2FixtureDef rect2990_fdef;
rect2990_fdef.shape = &rect2990_ps;

rect2990_fdef.density = 1.0f;
rect2990_fdef.friction = 0.0f;
rect2990_fdef.restitution = 0.4f;
rect2990_fdef.filter.groupIndex = 0;

rect2990_body->CreateFixture(&rect2990_fdef);
bodylist.push_back(rect2990_body);
uData* rect2990_ud = new uData();
rect2990_ud->name = "TITLE";
rect2990_ud->fill.Set(float(0)/255,float(255)/255,float(0)/255);
rect2990_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect2990_ud->strokewidth=1.17477167f;
rect2990_body->SetUserData(rect2990_ud);

//
//rect2992
//
b2BodyDef rect2992_bdef;
rect2992_bdef.type = b2_dynamicBody;
rect2992_bdef.bullet = false;
rect2992_bdef.position.Set(-0.182337517144f,14.6468391918f);
rect2992_bdef.angle=-0.0f;
rect2992_bdef.linearDamping = 0.3f;
rect2992_bdef.angularDamping =0.3f;
b2Body* rect2992_body = m_world->CreateBody(&rect2992_bdef);

b2PolygonShape rect2992_ps;
rect2992_ps.SetAsBox(2.11488001521f, 2.11488001521f);

b2FixtureDef rect2992_fdef;
rect2992_fdef.shape = &rect2992_ps;

rect2992_fdef.density = 1.0f;
rect2992_fdef.friction = 0.0f;
rect2992_fdef.restitution = 0.4f;
rect2992_fdef.filter.groupIndex = 0;

rect2992_body->CreateFixture(&rect2992_fdef);
bodylist.push_back(rect2992_body);
uData* rect2992_ud = new uData();
rect2992_ud->name = "TITLE";
rect2992_ud->fill.Set(float(255)/255,float(0)/255,float(255)/255);
rect2992_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect2992_ud->strokewidth=1.17477167f;
rect2992_body->SetUserData(rect2992_ud);

//
//rect2991
//
b2BodyDef rect2991_bdef;
rect2991_bdef.type = b2_dynamicBody;
rect2991_bdef.bullet = false;
rect2991_bdef.position.Set(-4.42583004734f,14.6468391918f);
rect2991_bdef.angle=-0.0f;
rect2991_bdef.linearDamping = 0.3f;
rect2991_bdef.angularDamping =0.3f;
b2Body* rect2991_body = m_world->CreateBody(&rect2991_bdef);

b2PolygonShape rect2991_ps;
rect2991_ps.SetAsBox(2.11488001521f, 2.11488001521f);

b2FixtureDef rect2991_fdef;
rect2991_fdef.shape = &rect2991_ps;

rect2991_fdef.density = 1.0f;
rect2991_fdef.friction = 0.0f;
rect2991_fdef.restitution = 0.4f;
rect2991_fdef.filter.groupIndex = 0;

rect2991_body->CreateFixture(&rect2991_fdef);
bodylist.push_back(rect2991_body);
uData* rect2991_ud = new uData();
rect2991_ud->name = "TITLE";
rect2991_ud->fill.Set(float(200)/255,float(55)/255,float(55)/255);
rect2991_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect2991_ud->strokewidth=1.17477167f;
rect2991_body->SetUserData(rect2991_ud);
