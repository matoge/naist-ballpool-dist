// True Type Font rendering
#include <FTGL/ftgl.h>


#if !defined FONT_FILE
#   ifdef WIN32
#       define FONT_FILE "fonts/ActionIs.ttf"
#   else
        // Put your font file here if configure did not find it.
#       define FONT_FILE 0
#   endif
#endif


#define FTGL_BITMAP 0
#define FTGL_PIXMAP 1
#define FTGL_OUTLINE 2
#define FTGL_POLYGON 3
#define FTGL_EXTRUDE 4
#define FTGL_TEXTURE 5
#define FTGL_BUFFER 6

char const* fontfile = FONT_FILE;
int current_font = FTGL_EXTRUDE;

static FTFont* fonts[7];
static FTPixmapFont* infoFont;
//wchar_t myString[16] = { 0x6FB3, 0x9580};
char myString[4096];

void setUpFonts(const char* file)
{
    fonts[FTGL_BITMAP] = new FTBitmapFont(file);
    fonts[FTGL_PIXMAP] = new FTPixmapFont(file);
    fonts[FTGL_OUTLINE] = new FTOutlineFont(file);
    fonts[FTGL_POLYGON] = new FTPolygonFont(file);
    fonts[FTGL_EXTRUDE] = new FTExtrudeFont(file);
    fonts[FTGL_TEXTURE] = new FTTextureFont(file);
    fonts[FTGL_BUFFER] = new FTBufferFont(file);

    for(int x = 0; x < 7; ++x)
    {
        if(fonts[x]->Error())
        {
            fprintf(stderr, "Failed to open font %s", file);
            exit(1);
        }

        if(!fonts[x]->FaceSize(30))
        {
            fprintf(stderr, "Failed to set size");
            exit(1);
        }

        fonts[x]->Depth(3.);
        fonts[x]->Outset(-.5, 1.5);

        fonts[x]->CharMap(ft_encoding_unicode);
    }

    infoFont = new FTPixmapFont(file);

    if(infoFont->Error())
    {
        fprintf(stderr, "Failed to open font %s", file);
        exit(1);
    }

    infoFont->FaceSize(18);
    strcpy(myString, "OpenGL is a powerful software interface for graphics "
           "hardware that allows graphics programmers to produce high-quality "
           "color images of 3D objects.\nabc def ghij klm nop qrs tuv wxyz "
           "ABC DEF GHIJ KLM NOP QRS TUV WXYZ 01 23 45 67 89");
}

